// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => (response.json()))
.then((json) => console.log(json))

// ------------------------------
// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

async function fetchData(){
    let result = await fetch("https://jsonplaceholder.typicode.com/todos")

    let json = await result.json()

    let map = json.map((todos => todos.title))
    console.log(map)
}
fetchData()

// --------------------------------
//  Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => (response.json()))
.then((json) => console.log(json))

// -----------------------------
// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => (response.json()))
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// -----------------------------
// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Create To Do List Item",
        userId: 1,
        completed: false
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// -----------------------------
// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated title using PUT method",
        userId: 1,
        completed: false
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


// ---------------------------------
// Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        completed: "Pending",
        title: "Updated To do List Item",
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// -----------------------------
// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated title using Patch method"
    })
})
.then((response => response.json()))
.then((json) => console.log(json))

// -----------------------------
// Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: "complete",
        dateCompleted: "07/09/21"
    })
})
.then((response => response.json()))
.then((json) => console.log(json))

// ------------------------------
// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE"
});
